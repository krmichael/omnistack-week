import React, { Component } from 'react';
import { MdInsertDriveFile } from 'react-icons/md';
import { distanceInWords } from 'date-fns';
import pt from 'date-fns/locale/pt';
import Dropzone from 'react-dropzone';
import socket from 'socket.io-client';
import PropTypes from 'prop-types';

import api from '../../services/api';
import logo from '../../assets/logo.svg';
import './styles.css';

export default class Box extends Component {
  state = {
    box: {}
  };

  async componentDidMount() {
    this.subscribeToNewFiles();

    const {
      match: {
        params: { id }
      }
    } = this.props;

    const response = await api.get(`/boxes/${id}`);

    this.setState({ box: response.data });
  }

  subscribeToNewFiles = () => {
    const {
      match: {
        params: { id }
      }
    } = this.props;

    const io = socket('https://rocketbox--api.herokuapp.com');

    io.emit('connectRoom', id);

    io.on('file', (data) => {
      this.setState((state) => ({
        box: { ...state.box, files: [data, ...state.box.files] }
      }));
    });
  };

  handleUpload = (files) => {
    files.forEach((file) => {
      const data = new FormData();

      const {
        match: {
          params: { id }
        }
      } = this.props;

      data.append('file', file);

      api.post(`/boxes/${id}/files`, data);
    });
  };

  render() {
    const { box } = this.state;

    return (
      <div id='box-container'>
        <header>
          <img src={logo} alt='logo' />
          <h1>{box.title}</h1>
        </header>

        <Dropzone onDropAccepted={this.handleUpload}>
          {({ getRootProps, getInputProps }) => (
            <div className='upload' {...getRootProps()}>
              <input {...getInputProps()} />

              <p>Arraste arquivos ou clique aqui</p>
            </div>
          )}
        </Dropzone>

        <ul>
          {box.files &&
            box.files.map((file) => (
              <li key={file._id}>
                <a
                  className='fileInfo'
                  href={file.url}
                  rel='noopener noreferrer'
                  target='_blank'>
                  <MdInsertDriveFile size={24} color='#A5Cfff' />
                  <strong>{file.title}</strong>
                </a>
                <span>
                  há
                  {' '}
                  {distanceInWords(file.createdAt, new Date(), {
                    locale: pt
                  })}
                </span>
              </li>
            ))}
        </ul>
      </div>
    );
  }
}

Box.propTypes = {
  match: PropTypes.shape({
    match: PropTypes.object
  }).isRequired
};
