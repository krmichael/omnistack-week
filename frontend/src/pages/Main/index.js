import React, { Component } from 'react';
import PropTypes from 'prop-types';
import logo from '../../assets/logo.svg';
import './styles.css';
import api from '../../services/api';

export default class Main extends Component {
  state = {
    newBox: ''
  };

  handleSubmit = async (e) => {
    e.preventDefault();

    const { newBox } = this.state;
    const { history } = this.props;

    const response = await api.post('/boxes', {
      title: newBox
    });

    history.push(`/box/${response.data._id}`);
  };

  handleInputChange = (e) => {
    this.setState({ newBox: e.target.value });
  };

  render() {
    const { newBox } = this.state;

    return (
      <div id='main-container'>
        <form onSubmit={this.handleSubmit}>
          <img src={logo} alt='logo' />
          <input
            placeholder='Criar um box'
            value={newBox}
            onChange={this.handleInputChange} />
          <button type='submit'>Criar</button>
        </form>
      </div>
    );
  }
}

Main.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired
};
